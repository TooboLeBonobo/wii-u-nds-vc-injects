# Wii U NDS VC Injects

A small tool to make NDS VC injects for Wii U.

Should be Linux friendly through Wine.



## How to 

- Put your base in a folder called Base on root of the exe.

- Make sure your base is decrypted.

- If not, decrypt it using [CDecrypt](https://github.com/VitaSmith/cdecrypt)

- Put your NDS ROM on root of the exe.

- Follow the instructions.

